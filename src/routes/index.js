import Header from "../templates/Header";
import Login from "../pages/Login";
import User from "../pages/User";
import Error404 from "../pages/Error404";
import getHash from "../utils/getHash";
import getData from "../utils/getData";
import resolveRoutes from "../utils/resolveRoutes";
import validateLogin from "../utils/validateLogin";
import Account from "../pages/Account";

const routes = {
  "/": Login,
  "/:user": User,
  "/:user/:accnum": Account,
};

const router = async () => {
  const header = null || document.getElementById("header");
  const content = null || document.getElementById("content");

  header.innerHTML = await Header();
  let hash = getHash();
  let route = await resolveRoutes(hash);
  let render = routes[route] ? routes[route] : Error404;

  content.innerHTML = await render();

  const IDinput = null || document.getElementById("userID");
  if (IDinput)
    IDinput.addEventListener("keypress", (e) => {
      if (e.keyCode === 32) {
        e.preventDefault();
      }
    });
  const LoginButton = null || document.getElementById("LoginButton");
  if (LoginButton) LoginButton.addEventListener("click", validateLogin);

  if (route === "/:user") {
    const userData = await getData(hash[1]);
    let list_items = userData.accounts.filter(
      (acc) =>
        !isNaN(acc.n) &&
        acc.n.trim() &&
        !isNaN(acc.saldo) &&
        (acc.tipo_letras.toUpperCase() == "CC" ||
          acc.tipo_letras.toUpperCase() == "CA")
    );
    list_items = list_items.map(
      (account) => `

              <a href="./#/${hash[1]}/#/${account.n}">
              <span>
                ${
                  account.tipo_letras.toUpperCase() == "CC"
                    ? "Cuenta Corriente"
                    : account.tipo_letras.toUpperCase() == "CA"
                    ? "Caja de Ahorro"
                    : account.tipo_letras.toUpperCase() == "CCP"
                    ? "Cuenta Corriente en Pesos"
                    : account.tipo_letras
                }
              </span>
              <span>
                Nro: ${account.n}
              </span>
              </a>

          `
    );
    const list_element = document.getElementById("AccountsContainer");
    const pagination_element = document.getElementById("pagination");

    let current_page = 1;
    let elements = 6;

    function DisplayList(items, wrapper, elements_per_page, page) {
      wrapper.innerHTML = "";
      page--;

      let start = elements_per_page * page;
      let end = start + elements_per_page;
      let paginatedItems = items.slice(start, end);

      for (let i = 0; i < paginatedItems.length; i++) {
        let item = paginatedItems[i];

        let item_element = document.createElement("article");
        item_element.classList.add("Account");
        item_element.innerHTML = item;
        wrapper.appendChild(item_element);
      }
    }

    function SetupPagination(items, wrapper, elements_per_page) {
      wrapper.innerHTML = "";

      let page_count = Math.ceil(items.length / elements_per_page);
      for (let i = 1; i < page_count + 1; i++) {
        let btn = PaginationButton(i, items);
        wrapper.appendChild(btn);
      }
    }

    function PaginationButton(page, items) {
      let button = document.createElement("button");
      button.innerHTML = page;

      if (current_page == page) button.classList.add("active");

      button.addEventListener("click", function () {
        current_page = page;
        DisplayList(items, list_element, elements, current_page);

        let current_btn = document.querySelector(".pagenumbers button.active");
        current_btn.classList.remove("active");

        button.classList.add("active");
      });

      return button;
    }

    DisplayList(list_items, list_element, elements, current_page);
    SetupPagination(list_items, pagination_element, elements);
  }
};

export default router;
