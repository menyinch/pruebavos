import router from "./routes";

window.addEventListener("load", router);
window.addEventListener("hashchange", router);

// fetch("../endpoint.txt")
//   .then((response) => response.json())
//   .then((data) => console.log(data));
