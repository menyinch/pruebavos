const getHash = () => location.hash.slice(1).split("/");

export default getHash;
