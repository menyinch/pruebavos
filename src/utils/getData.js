const API = "../../endpoint.txt";

const getData = async (user) => {
  if (user) {
    try {
      const response = await fetch(API);
      const data = await response.json();

      return data[user];
    } catch (error) {
      console.log("Fetch Error", error);
    }
  } else {
    try {
      const response = await fetch(API);
      const data = await response.json();

      return data;
    } catch (error) {
      console.log("Fetch Error", error);
    }
  }
};

export default getData;
