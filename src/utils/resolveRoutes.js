const resolveRoutes = (route) => {
  if (route.length === 3) {
    let validRoute = route === "/" ? route : "/:user";

    return validRoute;
  } else if (route.length === 4) {
    let validRoute = route === "/" ? route : "/:user/:accnum";

    return validRoute;
  }

  return `/${route}`;
};

export default resolveRoutes;
