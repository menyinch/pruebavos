import getData from "./getData";

const validateLogin = async () => {
  const userID = document.getElementById("userID").value;
  const password = document.getElementById("password").value;

  if (!userID || !password) {
    swal(
      "Campo Vacío!",
      "Debe completar ambos campos para iniciar sesión!",
      "error"
    );

    !userID
      ? (document.getElementById("userID").style.borderColor = "red")
      : (document.getElementById("userID").style.borderColor = "#0000001a");

    !password
      ? (document.getElementById("password").style.borderColor = "red")
      : (document.getElementById("password").style.borderColor = "#0000001a");
  } else {
    document.getElementById("userID").style.borderColor = "#0000001a";
    document.getElementById("password").style.borderColor = "#0000001a";
    const data = await getData();
    let userExist;
    for (let user in data) {
      if (data[user].idnum === userID && data[user].clave === password) {
        userExist = data[user];
        location.href = "./#/" + user + "/";
        break;
      }
    }

    if (!userExist) {
      swal(
        "Datos Incorrectos!",
        "Verifique sus datos e intente de nuevo!",
        "error"
      );
      document.getElementById("userID").value = "";
      document.getElementById("password").value = "";
    }
  }
};

export default validateLogin;
