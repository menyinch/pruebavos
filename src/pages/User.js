import getHash from "../utils/getHash";
import getData from "../utils/getData";

const User = async () => {
  const user = getHash();
  const userData = await getData(user[1]);
  const view = `
    <div class="UserDataContainer">
      <h5>
        Usuario: ${userData.idnum}
      </h5>
      <h3 class="UserDataTitle">
        Consulta de Saldo
      </h3>
      <h2 class="UserDataCTA">
        Selecciona la Cuenta a Consultar
      </h2>
        <div class="AccountsContainer" id="AccountsContainer">
          
        </div>
        <div class="pagenumbers" id="pagination"></div>
        <a href="/" class="BackButton">
          Salir
      </a>
    </div>
  `;

  return view;
};

export default User;
