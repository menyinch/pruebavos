const Login = () => {
  const view = `
    <div class="LoginFormContainer">
      <form class="loginForm">
        <h2>
          Inicio de Sesión
        </h2>
        <label class="labelUserID" for="userID">
          Usuario
        </label>
        <input id="userID" name="userID" maxlength="10">
        </input>
        <label class="labelPassword" for="password">
          Clave
        </label>
        <input id="password" type="password" name="password" maxlength="10">
        </input>
        <span class="validationError"></span>
        <input type="button" class="LoginButton" id="LoginButton" value="INGRESAR"></input> 
      </form>
    </div>
  `;

  return view;
};

export default Login;
