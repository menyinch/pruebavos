import getHash from "../utils/getHash";
import getData from "../utils/getData";

const Account = async () => {
  const user = getHash();
  const userData = await getData(user[1]);
  let account = userData.accounts.find((acc) => acc.n === user[3]);

  const view = `
    <div class="AccountDetailsContainer">
      <h3 class="UserDataTitle">
        Consulta de Saldo
      </h3>
      <h2 class="UserDataCTA">
        Este es tu saldo actual
      </h2>
      <article class="AccountDetails">
        <span>
          Saldo de la Cuenta: ${account.saldo}
        </span>
        <span>
          Tipo de Cuenta: ${
            account.tipo_letras.toUpperCase() == "CC"
              ? "Cuenta Corriente"
              : account.tipo_letras.toUpperCase() == "CA"
              ? "Caja de Ahorro"
              : account.tipo_letras.toUpperCase() == "CCP"
              ? "Cuenta Corriente en Pesos"
              : account.tipo_letras
          }
        </span>
        <span>
          Numero de Cuenta: ${account.n}
        </span>
      </article>
      <a href="./#/${user[1]}/" class="BackButton">
          Salir
      </a>
    </div>
  `;
  return view;
};

export default Account;
